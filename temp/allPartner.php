<!DOCTYPE html>
<html lang="fr-FR">

<!-- Auteur : GILLES nicolas,BASTARDO jeremie, GALLAND jean-brice,SOULA martine, DOSSANTOS mario-->
<!-- Modification : NIZARD alain,SOULA martine,GALLAND jean-brice,BASTARDO jeremie,SOULA cindy-->

  <head>

    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    
    <!-- Bootstrap -->   
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>

    <!-- CSS -->
    <link rel="stylesheet" href="css/style.css">

    <title>Lodecom</title>

  </head>

  <body>

    <!--Container menu des filtres -->
    <div id="app" class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card bg-light m-1"><!--changement here-->
            <div class="row">
              <div class="col-md-12">
                <div class="card-body">
                  <form action="switch.php?allPartner=set" method="post"> 
                    <!--   <label for="exampleDataList" class="form-label">NOM DU CLIENT</label>-->
                    <!--barre de recherche-->
                    <div class="input-group rounded">
                      <span class="input-group-text border-0" id="search-addon"><i class="fas fa-search"></i></span>
                      <input type="search" class="form-control rounded" placeholder="Entrez un client_id ou un client_name..." name="client_name" id="nomClient" list="datalistOptions"/>
                    </div>

                  </form>

                </div>
              </div>
            </div>
            
            <!--boutons actifs non actifs-->
            <div class="row"><!--changement here-->
              
                <div class="d-grid gap-2 mx-auto" ><!--changement here-->
                  <form action="" class="col-12 text-center" method="POST"><!--changement here-->
                    
                    <div class="transglo form-check form-check-inline"><!--changement here-->
                      <input class="form-check-input" type="radio" id="all" name="filter" value="all" checked>
                      <label class="form-check-label" for="all">Toutes</label>
                    </div>

                    <div class="transglo form-check form-check-inline"><!--changement here-->
                      <input class="form-check-input" type="radio" id="active" name="filter" value="active">
                      <label class="form-check-label" for="active">Actives</label>
                    </div>

                    <div class="transglo form-check form-check-inline"><!--changement here-->
                      <input class="form-check-input" type="radio" id="inactive" name="filter" value="inactive">
                      <label class="form-check-label" for="inactive">Inactives</label>
                    </div>

                    <div class="transglo form-check form-check-inline"><!--changement here-->
                      <input class="filtre form-check-button" type="submit" value="Filtrer"><!--changement here-->
                    </div>
                  </form>
                </div>
              
            </div>


          </div>
        </div>

        <!--Liste des cartes-->
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12 col-xl-6" v-for="Partenaire in mesdata">
              <div class="card bg-light border-success">
                <div class="row">
                  <div class="col-md-5">
                    <a :href=" Partenaire.url ">
                      <img :src="Partenaire.logo_url" class="card-img-top h-80" alt="...">
                    </a>
                    <form :id="'droit_s' + Partenaire.client_id" method="POST" :action="'switch.php?allPartner=set&client_id='+Partenaire.client_id">
                      <div class="custom-control custom-switch" v-if="Partenaire.active == 'Y'">
                        <input v-on:click="confirmation('desactiver',$event, Partenaire.client_id)" type="checkbox" class="custom-control-input" :id="'customSwitch'+Partenaire.client_id" checked>
                        <input type="hidden" name="active_struct" value="N">
                        <input type="hidden" name="client_id" :value="Partenaire.client_id">
                        <label class="custom-control-label" :for="'customSwitch'+Partenaire.client_id">Active</label>
                      </div>

                      <div class="custom-control custom-switch" v-else>
                        <input v-on:click="confirmation('activer',$event, Partenaire.client_id)" type="checkbox" class="custom-control-input" :id="'customSwitch'+Partenaire.client_id">
                        <input type="hidden" name="active_struct" value="Y">
                        <input type="hidden" name="client_id" :value="Partenaire.client_id">
                        <label class="custom-control-label" :for="'customSwitch'+Partenaire.client_id">Inactive</label>
                      </div>
                    </form>
                  </div>

                  <div class="col-md-7">
                    <div class="card-body">
                      <h5 class="card-title">{{ Partenaire.client_id }}</h5>
                      <h4 class="card-text">{{ Partenaire.client_name }}</h4>
                      <p class="card-text">{{ Partenaire.short_description }}</p>
                      <a :href="'switch.php?allStructure=set&client_id='+Partenaire.client_id" id="club"><button class="btn btn-success">Gérer les clubs</button></a>
                      <a :href="'switch.php?infoPartner=set&client_id='+Partenaire.client_id" id="info"><button class="btn btn-success">Plus d'infos</button></a>  
                    </div>
                  </div>
                </div>
              </div>
            </div>    
          </div>
        </div>

        <div class="container-fluid">
          <nav aria-label="Page navigation example">
            <ul class="pagination justify-content-center">
              <li class="page-item disabled"><a class="page-link" href="#" tabindex="-1">Previous</a></li>
              <li class="page-item "><a class="page-link text-success " href="#">1</a></li>
              <li class="page-item"><a class="page-link text-success" href="#">2</a></li>
              <li class="page-item"><a class="page-link text-success" href="#">3</a></li>
              <li class="page-item"><a class="page-link text-success" href="#">Next</a></li>
            </ul>
          </nav>
        </div>
      </div>
    </div>

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.12"></script>
    
    <script>

      var mesdata = <?php echo json_encode($data); ?>;

        // var mesdata = {
        //     Partenaires: [{
        //         logo_url: "https://upload.wikimedia.org/wikipedia/commons/a/a3/Basic-Fit_logo.png",
        //         client_id: "246824",
        //         client_name: "EasyGym",
        //         short_description: "EasyGym leader de la gym",
        //         url: "https://www.easygym.fr/",
        //         actif: "Y"
        //     },
        
        
        //     {
        //         logo_url: "https://upload.wikimedia.org/wikipedia/commons/a/a3/Basic-Fit_logo.png",
        //         client_id: "627422",
        //         client_name: "BasicFit",
        //         short_description: "BasicFit premier de la gymnastique olympique",
        //         url: "https://www.easygym.fr/",
        //         actif: "N"
        //     },
        
        //     {
        //         logo_url: "https://upload.wikimedia.org/wikipedia/commons/a/a3/Basic-Fit_logo.png",
        //         client_id: "845824",
        //         client_name: "Natafit",
        //         short_description: "BasicFit premier de la gymnastique de la natation",
        //         url: "https://www.easygym.fr/",
        //         actif: "Y"
        
        //     },
        //     {
        //         logo_url: "https://upload.wikimedia.org/wikipedia/commons/a/a3/Basic-Fit_logo.png",
        //         client_id: "627422",
        //         client_name: "BasicFit",
        //         short_description: "BasicFit premier de la gymnastique olympique",
        //         url: "https://www.easygym.fr/",
        //         actif: "Y"
        
        //     },
        //     {
        //         logo_url: "https://upload.wikimedia.org/wikipedia/commons/a/a3/Basic-Fit_logo.png",
        //         client_id: "627422",
        //         client_name: "BasicFit",
        //         short_description: "BasicFit premier de la gymnastique olympique",
        //         url: "https://www.easygym.fr/",
        //         actif: "Y"
        
        //     },
        //     {
        //         logo_url: "https://upload.wikimedia.org/wikipedia/commons/a/a3/Basic-Fit_logo.png",
        //         client_id: "627422",
        //         client_name: "BasicFit",
        //         short_description: "BasicFit premier de la gymnastique olympique",
        //         url: "https://www.easygym.fr/",
        //         actif: "Y"
        
        //     }
        // ]
        
        // };
  
      var app = new Vue({
        el: "#app",
        data: function() {
          return {
            mesdata
          };
        },
          
        methods: {
          confirmation: function(message, event, id) {
            result = confirm("etes vous sur de vouloir " + message);
            if (result) {
              document.forms["droit_s" + id].submit();
            } else {
              event.preventDefault();
              event.stopPropagation();
            }
          }
        }   
      })
    
    </script>

  </body>

</html>